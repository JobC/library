var less = require('gulp-less');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');
var pump = require('pump');
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var merge = require('merge-stream');

gulp.task('style', function () {
    var lessStream = gulp.src([ 'less/core.less' ])
            .pipe(less())
            .pipe(concat('less-files.less'))
        ;

    var scssStream = gulp.src([ 'sass/app.scss' ])
            .pipe(sass())
            .pipe(concat('scss-files.scss'))
        ;

    var mergedStream = merge(scssStream, lessStream)
        .pipe(concat('core.min.css'))
        .pipe(cssmin())
        .pipe(gulp.dest('./css/'))

    return mergedStream;
});

gulp.task('js', function (cb) {

    pump([ gulp.src([
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/owl.carousel/dist/owl.carousel.min.js',
            'node_modules/bootstrap/js/collapse.js',
            'node_modules/bootstrap/js/transition.js',
            'js/home.js'
        ])
            .pipe(concat('app.min.js')),
            uglify(),
            gulp.dest('js/')
        ],
        cb
    );
});