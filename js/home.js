/** Header* */
var $header = $('#header-top');
$(window).scroll(function () {
    'use strict';
    var scroll = $(this).scrollTop();
    if (scroll <= 100) {
        $header.removeClass("white");
    } else {
        $header.addClass("white");
    }
});

/** Slider **/
$slider = $('.owl-carousel');
$slider.owlCarousel({
    loop: true,
    margin: 10,
    nav: false,
    dots: false,
    items: 1,
    autoplay: false
});

/** Go next **/
$goNext = $(".go-next")
$goNext.on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({
        scrollTop: $("#featured").offset().top
    }, 450);
})
